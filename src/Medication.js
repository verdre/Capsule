/* exported Medication newFromExport */
const { Gio, GLib, GObject } = imports.gi;

var Medication = new GObject.registerClass({
    Signals: {
    },
    Properties: {
        'name': GObject.ParamSpec.string(
            'name',
            'Medication Name', 'Medication name in string',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT,
            null),
    },
}, class Medication extends GObject.Object {
    get name() {
        return this._name;
    }

    set name(name) {
        if (this._name === name)
            return;

        this._name = name;

        this.notify('name');
    }

    export() {
        return {
            name: this._name,
        };
    }
});

function newFromExport(obj) {
    return new Medication({
        name: obj.name,
    });
}
